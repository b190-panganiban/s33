
fetch("https://jsonplaceholder.typicode.com/todos")

.then(response => response.json())
.then( json => console.log(json));



async function fetchData() {
	let result = await fetch("https://jsonplaceholder.typicode.com/todos");
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json);
};

fetchData();
console.log("Hello");

// POST
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

// PUT
	fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "PUT",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
			id: 1,
			title:"Updated Post",
			body: "Hello Again",
			userId: 1
		})
	} )
	.then(response => response.json())
	.then(json => console.log(json));

// PATCH
	fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: "PATCH",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
			title:"Corrected Post"
		})
	} )
	.then(response => response.json())
	.then(json => console.log(json));

// DELETE
	fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: "DELETE"
	} )


	fetch("https://jsonplaceholder.typicode.com/todos?userId=1")
	.then(response => response.json())
	.then(json => console.log(json));


	fetch("https://jsonplaceholder.typicode.com/todos/1/comments")
	.then(response => response.json())
	.then(json => console.log(json));